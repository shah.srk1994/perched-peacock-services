# Welcome to Perched Peacock Services

[http://perchedpeacockservices.ap-south-1.elasticbeanstalk.com/](http://perchedpeacockservices.ap-south-1.elasticbeanstalk.com/)

![Pipeline status](https://gitlab.com/shah.srk1994/perched-peacock-services/badges/master/pipeline.svg)

Car parking is a major problem in urban areas in both developed and developing countries. Following the rapid incense of car ownership, many cities are suffering from lacking of car parking areas with imbalance between parking supply and demand which can be considered the initial reason for metropolis parking problems.
Perched Peacock aims to solve this problem by providing a real-time parking slot availibility information to its customers and let them book their parking spot in advance*.

## The Backend
The backend service of perched peacock is build using [Spring Boot](https://spring.io/) Framework.

## Getting Started

### Requirements

 1. Java 8 or later
 2. Maven 3.6.1 or later

To make sure its installed correctly, execute these command

	java -v
	mvn -v
These should print the current installed version respectively.

### Running it locally

To run the application locally, you need to build a jar using maven and then run it.

	git clone https://gitlab.com/shah.srk1994/perched-peacock-services.git
	cd perched-peacock-services
	mvn clean install
	cd target
	java -jar perched-peacock-services**.jar

You can then access the APIs using http://localhost:5000

##  Pipeline

The project pipeline is mainly divided into 3 stages:

#### 1. Build
   *  There are 2 jobs in this stage:
   * `build` - Source code is compilied and package is created
   * `test` - Unit and integration tests run to make sure no tests are breaking
#### 2. Deploy
   * There are 2 jobs in this stage:
   * `deploy to dev` -  deploys the code to dev environment. This job triggers only when a change is commited to the **develop** branch 
   * `deploy to prod` - deploys the code to prod environment. This job triggers only when a change is commited to the **master** branch 
#### 3. Advance
   * `sonar` Performs code quality check using sonar.

Checkout a pipeline [here](https://gitlab.com/shah.srk1994/perched-peacock-services/pipelines/88424800).

![Pipeline](images/Capture.JPG)

## Branching Stategy
For Branching stategy, we follow [Git Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows)

## Contribution
To contribute, please follow these simple workflow.

1. Make sure your changes are up to date with the deploy branch, If not please rebase the feature branch on develop.
2. Make sure the branch name follows convention of either `feature/newFeature` if it's a feature or `hotfix/fixPerformed` incase of hotfixes. 
3. Once your changes are merged, you can access them here [**non-prod**](http://perched-peacock-services-dev.ap-south-1.elasticbeanstalk.com/swagger-ui.html) and [**prod**](http://perchedpeacockservices.ap-south-1.elasticbeanstalk.com/swagger-ui.html)
