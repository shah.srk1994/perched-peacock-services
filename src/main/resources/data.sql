insert into parking_lot values(1, 'Alpine Eco Apartments, Doddannekundi','Bengaluru','India','Alpine Parking','Karnataka');
insert into parking_lot values(2, 'Prestige shantiniketan, whitefield','Bengaluru','India','Prestige parking','Karnataka');
insert into parking_lot values(3, 'Nirman nagar','Jaipur','India','Nirman parking','Karnataka');
insert into parking_lot values(4, 'Park avenue','New York City','USA','Avenue parking','New York');

insert into vehicle_type_info values (1, 'TWO_WHEELER',10,'INR',200,200);
insert into vehicle_type_info values (1, 'FOUR_WHEELER',20,'INR',100,100);

insert into vehicle_type_info values (2, 'TWO_WHEELER',20,'INR',400,400);
insert into vehicle_type_info values (2, 'FOUR_WHEELER',30,'INR',250,250);

insert into vehicle_type_info values (3, 'TWO_WHEELER',15,'INR',200,200);
insert into vehicle_type_info values (3, 'FOUR_WHEELER',25,'INR',200,200);

insert into vehicle_type_info values (4, 'TWO_WHEELER',2,'USD',500,500);
insert into vehicle_type_info values (4, 'FOUR_WHEELER',5,'USD',400,400);

insert into user_info values (1, 'password');
insert into user_info values (2, 'password');
insert into user_info values (3, 'password');
insert into user_info values (4, 'password');