package com.perchedpeacock.pechedpeacockservices.persistence.specification;

import com.perchedpeacock.pechedpeacockservices.model.SearchClauseType;
import com.perchedpeacock.pechedpeacockservices.model.SearchCriteria;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpecificationUtil {

    private static final String AND_CLAUSE_REGEX_PATTERN = "(\\w+?)([:~<>^])((\\w(,\\w)*)+)%";
    private static final String OR_CLAUSE_REGEX_PATTERN = "(\\w+)([:~<>^])((\\w(,\\w)*)+)#";


    public static void createSearchCriteria(SpecificationBuilder builder, String filter) {
        Pattern pattern = Pattern.compile(AND_CLAUSE_REGEX_PATTERN);
        String suffixChar = "%";
        filter = filter + "";
        if (filter.contains("%")) {
            pattern = Pattern.compile(AND_CLAUSE_REGEX_PATTERN);
            suffixChar = "%";
            builder.setSearchClauseType(SearchClauseType.AND);
        } else if (filter.contains("#")) {
            pattern = Pattern.compile(OR_CLAUSE_REGEX_PATTERN);
            suffixChar = "#";
            builder.setSearchClauseType(SearchClauseType.OR);
        }
        Matcher matcher = pattern.matcher(filter + suffixChar);
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
        }
    }

    public static Predicate generatePredicate(Path path, SearchCriteria searchCriteria, CriteriaBuilder criteriaBuilder) {

        Predicate predicate = criteriaBuilder.conjunction();

        Predicate predicateToAppend = predicate;

        switch (searchCriteria.getOperation()) {
            case "<":
                if (path.getJavaType() == Long.class) {
                    Long value = Long.parseLong(searchCriteria.getValue().toString());
                    predicateToAppend = criteriaBuilder.le(path, value);
                }
                break;
            case ">":
                if (path.getJavaType() == Long.class) {
                    Long value = Long.parseLong(searchCriteria.getValue().toString());
                    predicateToAppend = criteriaBuilder.ge(path, value);
                }
                break;
            case "^":
                if (path.getJavaType() == String.class) {
                    predicateToAppend = criteriaBuilder.equal(criteriaBuilder.lower(path),
                            searchCriteria.getValue().toString().toLowerCase());
                } else if (path.getJavaType() == Boolean.class) {
                    predicateToAppend = criteriaBuilder.equal(path,
                            Boolean.parseBoolean(searchCriteria.getValue().toString()));
                } else {
                    predicateToAppend = criteriaBuilder.equal(path, searchCriteria.getValue());
                }
                break;
            case "~":
                if (path.getJavaType() == String.class) {
                    predicateToAppend = criteriaBuilder.like(criteriaBuilder.lower(path),
                            "%" + searchCriteria.getValue().toString().toLowerCase() + "%");
                }
                break;
            case ":":
                String value = searchCriteria.getValue().toString();
                String[] list = value.split(",");

                if (path.getJavaType() == String.class) {
                    CriteriaBuilder.In<String> inExpression = criteriaBuilder.in(path);
                    for (String listitem : list) {
                        inExpression.value(listitem);
                    }
                    predicateToAppend = inExpression;
                } else if (path.getJavaType() == Long.class) {
                    CriteriaBuilder.In<Long> inExpression = criteriaBuilder.in(path);
                    for (String listitem : list) {
                        Long longValue = Long.parseLong(listitem);
                        inExpression.value(longValue);
                    }
                    predicateToAppend = inExpression;
                } else if (path.getJavaType() == Integer.class) {
                    CriteriaBuilder.In<Integer> inExpression = criteriaBuilder.in(path);
                    for (String listitem : list) {
                        Integer intValue = Integer.parseInt(listitem);
                        inExpression.value(intValue);
                    }
                    predicateToAppend = inExpression;
                }
                break;
            default:
                predicateToAppend = predicate;
                break;
        }
        predicate = appendPredicate(criteriaBuilder, predicate, predicateToAppend);
        return predicate;
    }

    private static Predicate appendPredicate(CriteriaBuilder criteriaBuilder, Predicate predicate, Predicate predicateToAppend) {
                return criteriaBuilder.and(predicate, predicateToAppend);
    }

}
