package com.perchedpeacock.pechedpeacockservices.persistence.specification;

import com.perchedpeacock.pechedpeacockservices.model.SearchClauseType;
import com.perchedpeacock.pechedpeacockservices.model.SearchCriteria;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

public class SpecificationBuilder<T> {

    private final List<SearchCriteria> params;
    private Class<T> type;

    @Setter
    private SearchClauseType searchClauseType;

    public SpecificationBuilder(Class<T> type) {
        this.type = type;
        this.params = new ArrayList<>();
        this.searchClauseType = SearchClauseType.AND;
    }

    public SpecificationBuilder<T> with(String key, String operation, Object value) {
        params.add(new SearchCriteria(key, operation, value));
        return this;
    }

    public Specification<T> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification<T>> specs = new ArrayList<>();
        params.forEach(param -> specs.add(new AppSpecification<T>(type, param)));

        Specification<T> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = searchClauseType.equals(SearchClauseType.AND)
                    ? Specification.where(result).and(specs.get(i))
                    : Specification.where(result).or(specs.get(i));
        }
        return result;
    }

}
