package com.perchedpeacock.pechedpeacockservices.persistence.specification;

import com.perchedpeacock.pechedpeacockservices.model.SearchCriteria;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

@AllArgsConstructor
public class AppSpecification<T> implements Specification<T> {

    private static final long serialVersionUID = 2024568291706236546L;

    private Class<T> type;

    private SearchCriteria searchCriteria;

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Path path = root.get(searchCriteria.getKey());
        return SpecificationUtil.generatePredicate(path, searchCriteria, criteriaBuilder);
    }

}
