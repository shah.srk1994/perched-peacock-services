package com.perchedpeacock.pechedpeacockservices.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
public class VehicleTypeInfo implements Serializable {
    private static final long serialVersionUID = 3532729689754602180L;

    @AttributeOverrides({
            @AttributeOverride(
                    name = "parkingLot",
                    column = @Column(name = "parking_lot_id")),
            @AttributeOverride(
                    name = "vehicleType",
                    column = @Column(name = "vehicleType"))
    })
    @EmbeddedId
    private VehicleInfoPK vehicleInfoPK;

    @JsonBackReference
    @MapsId("parkingLot")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parking_lot_id", nullable = false, insertable = false, updatable = false)
    private ParkingLot parkingLot;

    @Column
    private Double chargePerHour;

    @Column
    private String currency;

    @Column
    private Integer maxOccupancy;

    @Column
    private Integer vacancy;

}
