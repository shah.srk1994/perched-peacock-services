package com.perchedpeacock.pechedpeacockservices.persistence.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
public class UserInfo implements Serializable {
    private static final long serialVersionUID = 8637943174556693949L;

    @Column
    @Id
    @NotNull
    private Long userId;

    @Column
    @NotNull
    private String password;

}