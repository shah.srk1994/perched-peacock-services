package com.perchedpeacock.pechedpeacockservices.persistence.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@Entity
public class ParkingLot implements Serializable {

    private static final long serialVersionUID = 2358303447513023687L;

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String parkingLotName;

    @Column
    private String address;

    @Column
    private String city;

    @Column
    private String state;

    @Column
    private String country;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "parkingLot")
    private Set<VehicleTypeInfo> vehicleTypeInfoSet;

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ParkingLot)) {
            return false;
        }
        ParkingLot parkingLot = (ParkingLot) o;

        return Objects.equals(parkingLot.id, id) && Objects.equals(parkingLot.parkingLotName, parkingLotName)
                && Objects.equals(parkingLot.address, address) && Objects.equals(parkingLot.city, city)
                && Objects.equals(parkingLot.state, state) && Objects.equals(parkingLot.country, country)
                && Objects.equals(parkingLot.vehicleTypeInfoSet, vehicleTypeInfoSet) && super.equals(o);
    }
}
