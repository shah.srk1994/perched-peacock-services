package com.perchedpeacock.pechedpeacockservices.persistence.entity;

import com.perchedpeacock.pechedpeacockservices.model.VehicleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParkingLog implements Serializable {

    private static final long serialVersionUID = 6572347476614155205L;

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Long parkingLotId;

    @Column
    @Enumerated(EnumType.STRING)
    private VehicleType vehicleType;

    @Column
    private String vehicleNumber;

    @Column
    private Double vehicleWeight;

    @Column
    private Long enterTimeEpoch;

    @Column
    private Long exitTimeEpoch;

    @Column
    private Double charge;

    @Column
    private String currency;

}
