package com.perchedpeacock.pechedpeacockservices.persistence.entity;

import com.perchedpeacock.pechedpeacockservices.model.VehicleType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Embeddable
public class VehicleInfoPK implements Serializable {
    private static final long serialVersionUID = -2849654237415860949L;

    @NonNull
    @Enumerated(EnumType.STRING)
    private VehicleType vehicleType;

    private Long parkingLot;
}
