package com.perchedpeacock.pechedpeacockservices.persistence.repository;

import com.perchedpeacock.pechedpeacockservices.persistence.entity.ParkingLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ParkingLogRepository extends JpaRepository<ParkingLog, Long>, JpaSpecificationExecutor<ParkingLog> {
    ParkingLog getParkingLogByParkingLotIdAndVehicleNumberAndAndExitTimeEpoch(Long parkingLogId, String vehicleNumber, Long exitTime);

}
