package com.perchedpeacock.pechedpeacockservices.persistence.repository;

import com.perchedpeacock.pechedpeacockservices.persistence.entity.ParkingLot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ParkingLotRepository extends JpaRepository<ParkingLot, Long>, JpaSpecificationExecutor<ParkingLot> {

}
