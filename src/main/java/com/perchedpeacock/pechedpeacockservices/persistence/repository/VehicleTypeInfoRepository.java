package com.perchedpeacock.pechedpeacockservices.persistence.repository;

import com.perchedpeacock.pechedpeacockservices.persistence.entity.VehicleInfoPK;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.VehicleTypeInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VehicleTypeInfoRepository extends JpaRepository<VehicleTypeInfo, VehicleInfoPK>, JpaSpecificationExecutor<VehicleTypeInfo> {

}