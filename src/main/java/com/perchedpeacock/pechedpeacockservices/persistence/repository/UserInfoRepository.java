package com.perchedpeacock.pechedpeacockservices.persistence.repository;

import com.perchedpeacock.pechedpeacockservices.persistence.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {

}