package com.perchedpeacock.pechedpeacockservices.controller;

import com.perchedpeacock.pechedpeacockservices.exception.ParkingLotNotFoundException;
import com.perchedpeacock.pechedpeacockservices.model.AppResponseModel;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.ParkingLot;
import com.perchedpeacock.pechedpeacockservices.service.ParkingLotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RequestMapping("/api/v1")
@RestController
public class ParkingLotController {

    @Autowired
    private ParkingLotService parkingLotService;

    @GetMapping("/parking-lot")
    public ResponseEntity<AppResponseModel<ParkingLot>> fetchParkingLots(
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
            @RequestParam(value = "filter", required = false) String filter) {

        return new ResponseEntity<>(parkingLotService.getAllParkingLots(page, size, filter), HttpStatus.OK);
    }

    @GetMapping("/parking-lot/{id}")
    public ResponseEntity<ParkingLot> fetchParkingLotById(@Valid @PathVariable("id") Long id) throws ParkingLotNotFoundException {
        Optional<ParkingLot> parkingLotOptional = parkingLotService.getParkingLotById(id);
        if (!parkingLotOptional.isPresent()) {
            throw new ParkingLotNotFoundException("The requested parking lot not found.");
        }
        return new ResponseEntity<>(parkingLotOptional.get(), HttpStatus.OK);
    }

}
