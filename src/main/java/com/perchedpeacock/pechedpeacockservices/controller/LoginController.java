package com.perchedpeacock.pechedpeacockservices.controller;

import com.perchedpeacock.pechedpeacockservices.model.UserRequestParam;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.ParkingLot;
import com.perchedpeacock.pechedpeacockservices.service.ParkingLotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequestMapping("/api/v1")
@RestController
public class LoginController {

    @Autowired
    private ParkingLotService parkingLotService;

    @PostMapping("/login")
    public ResponseEntity<ParkingLot> login(@RequestBody @Valid UserRequestParam userRequestParam) {
        return new ResponseEntity<>(parkingLotService.getParkingLotById(userRequestParam.getUserId()).get(), HttpStatus.OK);
    }

}