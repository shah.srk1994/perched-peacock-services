package com.perchedpeacock.pechedpeacockservices.controller;

import com.perchedpeacock.pechedpeacockservices.model.AppResponseModel;
import com.perchedpeacock.pechedpeacockservices.model.ParkingLogResponse;
import com.perchedpeacock.pechedpeacockservices.model.ParkingRequestParams;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.ParkingLog;
import com.perchedpeacock.pechedpeacockservices.service.ParkingLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/parking-log")
@RestController
public class ParkingLogController {

    @Autowired
    private ParkingLogService parkingLogService;

    @GetMapping
    public ResponseEntity<AppResponseModel<ParkingLog>> getParkingLogs(
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
            @RequestParam(value = "filter", required = false) String filter) {

        return new ResponseEntity<>(parkingLogService.getAllParkingLogs(page, size, filter), HttpStatus.OK);

    }

    @PostMapping(value = "/parkVehicle")
    public ResponseEntity<ParkingLogResponse> parkVehicle(
            @RequestBody ParkingRequestParams parkRequest) {
        return parkingLogService.parkVehicle(parkRequest);
    }

    @PostMapping(value = "/exitVehicle")
    public ResponseEntity<ParkingLogResponse> exitVehicle(
            @RequestBody ParkingRequestParams parkRequest) {
        return parkingLogService.exitVehicle(parkRequest);
    }

}
