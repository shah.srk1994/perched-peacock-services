package com.perchedpeacock.pechedpeacockservices.service;

import com.perchedpeacock.pechedpeacockservices.model.AppResponseModel;
import com.perchedpeacock.pechedpeacockservices.model.ParkingLogResponse;
import com.perchedpeacock.pechedpeacockservices.model.ParkingRequestParams;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.ParkingLog;
import org.springframework.http.ResponseEntity;

public interface ParkingLogService {

    AppResponseModel<ParkingLog> getAllParkingLogs(Integer page, Integer size, String filter);

    ResponseEntity<ParkingLogResponse> parkVehicle(ParkingRequestParams parkRequest);

    ResponseEntity<ParkingLogResponse> exitVehicle(ParkingRequestParams parkRequest);

}