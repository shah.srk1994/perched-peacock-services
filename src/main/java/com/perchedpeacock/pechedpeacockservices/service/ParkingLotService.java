package com.perchedpeacock.pechedpeacockservices.service;

import com.perchedpeacock.pechedpeacockservices.model.AppResponseModel;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.ParkingLot;

import java.util.Optional;

public interface ParkingLotService {

    AppResponseModel<ParkingLot> getAllParkingLots(Integer page, Integer size, String filter);

    Optional<ParkingLot> getParkingLotById(Long id);

}
