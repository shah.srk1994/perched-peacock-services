package com.perchedpeacock.pechedpeacockservices.service;

import com.perchedpeacock.pechedpeacockservices.model.AppResponseModel;
import com.perchedpeacock.pechedpeacockservices.model.ParkingLogResponse;
import com.perchedpeacock.pechedpeacockservices.model.ParkingRequestParams;
import com.perchedpeacock.pechedpeacockservices.model.VehicleType;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.ParkingLog;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.VehicleInfoPK;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.VehicleTypeInfo;
import com.perchedpeacock.pechedpeacockservices.persistence.repository.ParkingLogRepository;
import com.perchedpeacock.pechedpeacockservices.persistence.repository.VehicleTypeInfoRepository;
import com.perchedpeacock.pechedpeacockservices.persistence.specification.SpecificationBuilder;
import com.perchedpeacock.pechedpeacockservices.persistence.specification.SpecificationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

import static com.perchedpeacock.pechedpeacockservices.model.ParkingResponseModel.*;

@Service
public class ParkingLogServiceImpl implements ParkingLogService {
    private static Long EMPTY_EXIT_TIME = -1L;

    @Autowired
    private ParkingLogRepository parkingLogRepository;

    @Autowired
    private VehicleTypeInfoRepository vehicleTypeInfoRepository;

    @Override
    public AppResponseModel<ParkingLog> getAllParkingLogs(Integer page, Integer size, String filter) {
        Pageable pageable = PageRequest.of(page, size);
        SpecificationBuilder<ParkingLog> builder = new SpecificationBuilder<>(ParkingLog.class);

        SpecificationUtil.createSearchCriteria(builder, filter);

        Specification<ParkingLog> specification = builder.build();
        Page<ParkingLog> parkingLotPage = parkingLogRepository.findAll(specification, pageable);
        return new AppResponseModel<>(parkingLotPage.getContent(), parkingLotPage.getTotalElements());
    }

    @Override
    public ResponseEntity<ParkingLogResponse> parkVehicle(final ParkingRequestParams parkRequest) {
        if (!doesParkingLotExist(parkRequest)) {
            return LOT_DOES_NOT_EXIST.getResponseEntity(parkRequest.getParkingLotId());
        }
        if (getParkedVehicle(parkRequest).isPresent()) {
            return VEHICLE_ALREADY_PARKED.getResponseEntity(parkRequest.getVehicleNumber(),
                    parkRequest.getParkingLotId());
        }
        if (!isParkingAvailable(parkRequest)) {
            return PARKING_LOT_FULL.getResponseEntity();
        }
        updateParkingLogTableOnEntry(parkRequest);
        updateParkingVacancy(getVehicleInfoPrimaryKey(parkRequest), -1);

        return PARKED_SUCCESSFULLY.getResponseEntity();
    }

    @Override
    public ResponseEntity<ParkingLogResponse> exitVehicle(final ParkingRequestParams parkRequest) {
        if (!doesParkingLotExist(parkRequest)) {
            return LOT_DOES_NOT_EXIST.getResponseEntity(parkRequest.getParkingLotId());
        }
        Optional<ParkingLog> parkingLog = getParkedVehicle(parkRequest);
        if (!parkingLog.isPresent()) {
            return VEHICLE_NOT_PARKED.getResponseEntity(parkRequest.getVehicleNumber(),
                    parkRequest.getParkingLotId());
        }
        VehicleTypeInfo vehicleTypeInfo = vehicleTypeInfoRepository.findById(getVehicleInfoPrimaryKey(parkRequest)).get();
        updateParkingLogTableOnExit(vehicleTypeInfo, parkingLog.get());
        updateParkingVacancy(getVehicleInfoPrimaryKey(parkRequest), 1);

        return EXITED_SUCCESSFULLY.getResponseEntity(parkingLog.get().getCharge(), parkingLog.get().getCurrency());
    }

    private boolean doesParkingLotExist(final ParkingRequestParams parkRequest) {
        return vehicleTypeInfoRepository.existsById(getVehicleInfoPrimaryKey(parkRequest));
    }

    private Optional<ParkingLog> getParkedVehicle(final ParkingRequestParams parkRequest) {
        ParkingLog parkingLog = parkingLogRepository.getParkingLogByParkingLotIdAndVehicleNumberAndAndExitTimeEpoch(
                parkRequest.getParkingLotId(),
                parkRequest.getVehicleNumber(),
                EMPTY_EXIT_TIME);
        return Optional.ofNullable(parkingLog);
    }

    private boolean isParkingAvailable(ParkingRequestParams requestParam) {
        Optional<VehicleTypeInfo> vehicleTypeInfoOptional = vehicleTypeInfoRepository.findById(getVehicleInfoPrimaryKey(requestParam));
        if (vehicleTypeInfoOptional.isPresent()) {
            VehicleTypeInfo vehicleTypeInfo = vehicleTypeInfoOptional.get();
            return vehicleTypeInfo.getVacancy() > 0;
        }
        return false;
    }

    private VehicleInfoPK getVehicleInfoPrimaryKey(ParkingRequestParams requestParam) {
        VehicleInfoPK pk = new VehicleInfoPK(VehicleType.valueOf(requestParam.getVehicleType()));
        pk.setParkingLot(requestParam.getParkingLotId());
        return pk;
    }

    private void updateParkingVacancy(final VehicleInfoPK primaryKey, int vacancy) {
        VehicleTypeInfo vehicleTypeInfo = vehicleTypeInfoRepository.findById(primaryKey).get();
        vehicleTypeInfo.setVacancy(vehicleTypeInfo.getVacancy() + vacancy);
        vehicleTypeInfoRepository.save(vehicleTypeInfo);
    }

    private void updateParkingLogTableOnEntry(final ParkingRequestParams parkRequest) {
        ParkingLog parkingLog = ParkingLog.builder()
                .enterTimeEpoch(new Date().getTime())
                .exitTimeEpoch(EMPTY_EXIT_TIME)
                .parkingLotId(parkRequest.getParkingLotId())
                .vehicleNumber(parkRequest.getVehicleNumber())
                .vehicleType(VehicleType.valueOf(parkRequest.getVehicleType()))
                .vehicleWeight(parkRequest.getVehicleWeight())
                .build();
        parkingLogRepository.save(parkingLog);
    }

    private void updateParkingLogTableOnExit(final VehicleTypeInfo vehicleTypeInfo,
                                             final ParkingLog parkLog) {
        parkLog.setExitTimeEpoch(new Date().getTime());

        // Reference: https://www.mkyong.com/java/how-to-calculate-date-time-difference-in-java/
        Long durationInMs = parkLog.getExitTimeEpoch() - parkLog.getEnterTimeEpoch();
        Long durationInHours = (long) (Math.ceil(durationInMs / (60.0 * 60.0 * 1000.0) % 24));

        parkLog.setCharge(vehicleTypeInfo.getChargePerHour() * durationInHours);
        parkLog.setCurrency(vehicleTypeInfo.getCurrency());

        parkingLogRepository.save(parkLog);
    }
}
