package com.perchedpeacock.pechedpeacockservices.service;

import com.perchedpeacock.pechedpeacockservices.persistence.entity.UserInfo;
import com.perchedpeacock.pechedpeacockservices.persistence.repository.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Override
    @Transactional
    public Optional<UserInfo> getUserInfoById(Long id) {
        return userInfoRepository.findById(id);
    }
}