package com.perchedpeacock.pechedpeacockservices.service;

import com.perchedpeacock.pechedpeacockservices.model.AppResponseModel;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.ParkingLot;
import com.perchedpeacock.pechedpeacockservices.persistence.repository.ParkingLotRepository;
import com.perchedpeacock.pechedpeacockservices.persistence.specification.SpecificationBuilder;
import com.perchedpeacock.pechedpeacockservices.persistence.specification.SpecificationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ParkingLotServiceImpl implements ParkingLotService {

    @Autowired
    private ParkingLotRepository parkingLotRepository;

    @Override
    public AppResponseModel<ParkingLot> getAllParkingLots(Integer page, Integer size, String filter) {
        Pageable pageable = PageRequest.of(page, size);
        SpecificationBuilder<ParkingLot> builder = new SpecificationBuilder<>(ParkingLot.class);

        SpecificationUtil.createSearchCriteria(builder, filter);

        Specification<ParkingLot> specification = builder.build();
        Page<ParkingLot> parkingLotPage = parkingLotRepository.findAll(specification, pageable);
        return new AppResponseModel<>(parkingLotPage.getContent(), parkingLotPage.getTotalElements());
    }

    @Override
    @Transactional
    public Optional<ParkingLot> getParkingLotById(Long id) {
        return parkingLotRepository.findById(id);
    }

}
