package com.perchedpeacock.pechedpeacockservices.service;

import com.perchedpeacock.pechedpeacockservices.persistence.entity.UserInfo;

import java.util.Optional;

public interface UserInfoService {

    Optional<UserInfo> getUserInfoById(Long id);

}