package com.perchedpeacock.pechedpeacockservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PechedPeacockServicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(PechedPeacockServicesApplication.class, args);
    }

}
