package com.perchedpeacock.pechedpeacockservices.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ParkingLogResponse {

    private String response;

}
