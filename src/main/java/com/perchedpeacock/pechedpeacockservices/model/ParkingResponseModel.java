package com.perchedpeacock.pechedpeacockservices.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

@AllArgsConstructor
@Getter
public enum ParkingResponseModel {
    LOT_DOES_NOT_EXIST("Invalid parking Lot Id : %d", BAD_REQUEST),
    VEHICLE_ALREADY_PARKED("Vehicle Number : %s already parked at lot: %d", BAD_REQUEST),
    VEHICLE_NOT_PARKED("Vehicle Number : %s is not parked at lot: %d", BAD_REQUEST),
    PARKING_LOT_FULL("Vehicle cannot be parked. Parking Lot full", OK),
    PARKED_SUCCESSFULLY("Vehicle parked successfully", OK),
    EXITED_SUCCESSFULLY("Vehicle exited successfully. Total charge: %.2f %s", OK);

    private String body;
    private HttpStatus httpStatus;

    public ResponseEntity<ParkingLogResponse> getResponseEntity(Object... parameters) {
        ParkingLogResponse parkingLogResponse = new ParkingLogResponse(String.format(body, parameters));
        return new ResponseEntity<>(parkingLogResponse, httpStatus);
    }
}
