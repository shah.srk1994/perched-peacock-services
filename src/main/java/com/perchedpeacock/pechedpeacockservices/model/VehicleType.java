package com.perchedpeacock.pechedpeacockservices.model;

public enum VehicleType {

    TWO_WHEELER, FOUR_WHEELER

}
