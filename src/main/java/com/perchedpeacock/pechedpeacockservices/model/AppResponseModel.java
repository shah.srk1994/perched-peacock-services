package com.perchedpeacock.pechedpeacockservices.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AppResponseModel<T> {

    List<T> dataList;
    Long totalCount;

}