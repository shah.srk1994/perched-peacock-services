package com.perchedpeacock.pechedpeacockservices.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ParkingRequestParams {
    private Long parkingLotId;
    private String vehicleType;
    private String vehicleNumber;
    private Double vehicleWeight;
}