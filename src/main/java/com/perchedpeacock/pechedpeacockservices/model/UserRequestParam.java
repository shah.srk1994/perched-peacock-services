package com.perchedpeacock.pechedpeacockservices.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserRequestParam {

    @NotNull
    private Long userId;

}