package com.perchedpeacock.pechedpeacockservices.model;

public enum SearchClauseType {

    AND, OR

}
