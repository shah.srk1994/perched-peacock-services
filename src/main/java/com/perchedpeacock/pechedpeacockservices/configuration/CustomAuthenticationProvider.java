package com.perchedpeacock.pechedpeacockservices.configuration;

import com.perchedpeacock.pechedpeacockservices.persistence.entity.UserInfo;
import com.perchedpeacock.pechedpeacockservices.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserInfoService userInfoService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String userId = authentication.getName();
        String password = authentication.getCredentials().toString();

        if (StringUtils.isEmpty(userId)) {
            throw new UsernameNotFoundException("No Account found for given user id.");
        }

        Optional<UserInfo> userInfoOptional = userInfoService.getUserInfoById(Long.parseLong(userId));

        if (!userInfoOptional.isPresent()) {
            throw new UsernameNotFoundException("No Account found for given user id.");
        }

        if (!userInfoOptional.get().getPassword().equals(password)) {
            throw new BadCredentialsException("Incorrect Credentials Provided.");
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("User")); // description is a string

        return new UsernamePasswordAuthenticationToken(userId, password, authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}