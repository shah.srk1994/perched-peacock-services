package com.perchedpeacock.pechedpeacockservices.exception;

public class ParkingLotNotFoundException extends Exception {
    private static final long serialVersionUID = -48745865280826070L;

    public ParkingLotNotFoundException(String message) {
        super(message);
    }

    public ParkingLotNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
