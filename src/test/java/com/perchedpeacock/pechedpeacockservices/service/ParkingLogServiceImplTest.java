package com.perchedpeacock.pechedpeacockservices.service;

import com.perchedpeacock.pechedpeacockservices.model.ParkingLogResponse;
import com.perchedpeacock.pechedpeacockservices.model.ParkingRequestParams;
import com.perchedpeacock.pechedpeacockservices.model.VehicleType;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.ParkingLog;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.VehicleInfoPK;
import com.perchedpeacock.pechedpeacockservices.persistence.entity.VehicleTypeInfo;
import com.perchedpeacock.pechedpeacockservices.persistence.repository.ParkingLogRepository;
import com.perchedpeacock.pechedpeacockservices.persistence.repository.VehicleTypeInfoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import static com.perchedpeacock.pechedpeacockservices.model.ParkingResponseModel.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
public class ParkingLogServiceImplTest {

    private final static Long PARKING_LOT_ID = 1L;
    private final static Double VEHICLE_WEIGHT = 200.0;
    private final static String VEHICLE_TYPE = "TWO_WHEELER";
    private final static String VEHICLE_NUMBER = "KA-09-7664";
    private final static Long EMPTY_EXIT_TIME = -1L;
    private final static int DEFAULT_VACANCY = 10;
    private final static Double CHARGE_PER_HOUR = 15.0;
    private final static String CURRENCY = "INR";
    private final static Date TEST_DATE = new Date();

    @InjectMocks
    private ParkingLogServiceImpl parkingLogService;

    @Mock
    private ParkingLogRepository parkingLogRepository;
    @Mock
    private VehicleTypeInfoRepository vehicleTypeInfoRepository;

    @Test
    public void parkVehicle_VacantLot_SUCCESS() {
        ParkingRequestParams param = getTestRequestParams();
        VehicleInfoPK pk = getVehicleInfoPrimaryKey(param);
        Mockito.when(vehicleTypeInfoRepository.existsById(pk)).thenReturn(true);
        Mockito.when(parkingLogRepository.getParkingLogByParkingLotIdAndVehicleNumberAndAndExitTimeEpoch(
                PARKING_LOT_ID, VEHICLE_NUMBER, -1L)).thenReturn(null);
        VehicleTypeInfo vehicleTypeInfo = getTestVehicleTypeInfoWithVacancy(DEFAULT_VACANCY);
        Mockito.when(vehicleTypeInfoRepository.findById(pk)).thenReturn(Optional.of(vehicleTypeInfo));

        ResponseEntity<ParkingLogResponse> responseEntity = parkingLogService.parkVehicle(param);

        verify(parkingLogRepository, times(1)).save(any(ParkingLog.class));
        verify(vehicleTypeInfoRepository, times(1)).save(getTestVehicleTypeInfoWithVacancy(DEFAULT_VACANCY - 1));
        assertThat(Objects.requireNonNull(responseEntity.getBody()).getResponse()).isEqualTo(PARKED_SUCCESSFULLY.getBody());
        assertThat(responseEntity.getStatusCode()).isEqualTo(PARKED_SUCCESSFULLY.getHttpStatus());
    }

    @Test
    public void parkVehicle_InvalidLotId_LotDoesntExistResponse() {
        ParkingRequestParams param = getTestRequestParams();
        Mockito.when(vehicleTypeInfoRepository.existsById(getVehicleInfoPrimaryKey(param))).thenReturn(false);

        ResponseEntity<ParkingLogResponse> responseEntity = parkingLogService.parkVehicle(param);

        verify(parkingLogRepository, times(0)).save(any(ParkingLog.class));
        verify(vehicleTypeInfoRepository, times(0)).save(any(VehicleTypeInfo.class));
        assertThat(Objects.requireNonNull(responseEntity.getBody()).getResponse()).isEqualTo(String.format(LOT_DOES_NOT_EXIST.getBody(), PARKING_LOT_ID));
        assertThat(responseEntity.getStatusCode()).isEqualTo(LOT_DOES_NOT_EXIST.getHttpStatus());
    }

    @Test
    public void parkVehicle_VehicleParked_VehicleParkedResponse() {
        ParkingRequestParams param = getTestRequestParams();
        Mockito.when(vehicleTypeInfoRepository.existsById(getVehicleInfoPrimaryKey(param))).thenReturn(true);
        Mockito.when(parkingLogRepository.getParkingLogByParkingLotIdAndVehicleNumberAndAndExitTimeEpoch(
                PARKING_LOT_ID, VEHICLE_NUMBER, -1L)).thenReturn(new ParkingLog());

        ResponseEntity<ParkingLogResponse> responseEntity = parkingLogService.parkVehicle(param);

        verify(parkingLogRepository, times(0)).save(any(ParkingLog.class));
        verify(vehicleTypeInfoRepository, times(0)).save(any(VehicleTypeInfo.class));
        assertThat(Objects.requireNonNull(responseEntity.getBody()).getResponse()).isEqualTo(String.format(VEHICLE_ALREADY_PARKED.getBody(),
                VEHICLE_NUMBER, PARKING_LOT_ID));
        assertThat(responseEntity.getStatusCode()).isEqualTo(VEHICLE_ALREADY_PARKED.getHttpStatus());
    }

    @Test
    public void parkVehicle_NoVacancy_LotFullResponse() {
        ParkingRequestParams param = getTestRequestParams();
        VehicleInfoPK pk = getVehicleInfoPrimaryKey(param);
        Mockito.when(vehicleTypeInfoRepository.existsById(pk)).thenReturn(true);
        Mockito.when(parkingLogRepository.getParkingLogByParkingLotIdAndVehicleNumberAndAndExitTimeEpoch(
                PARKING_LOT_ID, VEHICLE_NUMBER, -1L)).thenReturn(null);
        VehicleTypeInfo vehicleTypeInfo = getTestVehicleTypeInfoWithVacancy(0);
        Mockito.when(vehicleTypeInfoRepository.findById(pk)).thenReturn(Optional.of(vehicleTypeInfo));

        ResponseEntity<ParkingLogResponse> responseEntity = parkingLogService.parkVehicle(param);

        verify(parkingLogRepository, times(0)).save(any(ParkingLog.class));
        verify(vehicleTypeInfoRepository, times(0)).save(any(VehicleTypeInfo.class));
        assertThat(Objects.requireNonNull(responseEntity.getBody()).getResponse()).isEqualTo(String.format(PARKING_LOT_FULL.getBody(),
                VEHICLE_NUMBER, PARKING_LOT_ID));
        assertThat(responseEntity.getStatusCode()).isEqualTo(PARKING_LOT_FULL.getHttpStatus());
    }

    @Test
    public void exitVehicle_VehicleParked_SUCCESS() {
        ParkingRequestParams param = getTestRequestParams();
        ParkingLog log = getTestParkinglog(param);
        VehicleInfoPK pk = getVehicleInfoPrimaryKey(param);
        Mockito.when(vehicleTypeInfoRepository.existsById(pk)).thenReturn(true);
        Mockito.when(parkingLogRepository.getParkingLogByParkingLotIdAndVehicleNumberAndAndExitTimeEpoch(
                PARKING_LOT_ID, VEHICLE_NUMBER, -1L)).thenReturn(log);
        VehicleTypeInfo vehicleTypeInfo = getTestVehicleTypeInfoWithVacancy(DEFAULT_VACANCY);
        Mockito.when(vehicleTypeInfoRepository.findById(pk)).thenReturn(Optional.of(vehicleTypeInfo));

        ResponseEntity<ParkingLogResponse> responseEntity = parkingLogService.exitVehicle(param);

        verify(parkingLogRepository, times(1)).save(any(ParkingLog.class));
        verify(vehicleTypeInfoRepository, times(1)).save(getTestVehicleTypeInfoWithVacancy(DEFAULT_VACANCY + 1));
        assertThat(Objects.requireNonNull(responseEntity.getBody()).getResponse()).isEqualTo(String.format(EXITED_SUCCESSFULLY.getBody(), CHARGE_PER_HOUR, CURRENCY));
        assertThat(responseEntity.getStatusCode()).isEqualTo(EXITED_SUCCESSFULLY.getHttpStatus());
    }

    @Test
    public void exitVehicle_InvalidLotId_LotDoesntExistResponse() {
        ParkingRequestParams param = getTestRequestParams();
        Mockito.when(vehicleTypeInfoRepository.existsById(getVehicleInfoPrimaryKey(param))).thenReturn(false);

        ResponseEntity<ParkingLogResponse> responseEntity = parkingLogService.exitVehicle(param);

        verify(parkingLogRepository, times(0)).save(any(ParkingLog.class));
        verify(vehicleTypeInfoRepository, times(0)).save(any(VehicleTypeInfo.class));
        assertThat(Objects.requireNonNull(responseEntity.getBody()).getResponse()).isEqualTo(String.format(LOT_DOES_NOT_EXIST.getBody(), PARKING_LOT_ID));
        assertThat(responseEntity.getStatusCode()).isEqualTo(LOT_DOES_NOT_EXIST.getHttpStatus());
    }

    @Test
    public void exitVehicle_VehicleNotParked_VehicleNotParkedResponse() {
        ParkingRequestParams param = getTestRequestParams();
        Mockito.when(vehicleTypeInfoRepository.existsById(getVehicleInfoPrimaryKey(param))).thenReturn(true);
        Mockito.when(parkingLogRepository.getParkingLogByParkingLotIdAndVehicleNumberAndAndExitTimeEpoch(
                PARKING_LOT_ID, VEHICLE_NUMBER, -1L)).thenReturn(null);

        ResponseEntity<ParkingLogResponse> responseEntity = parkingLogService.exitVehicle(param);

        verify(parkingLogRepository, times(0)).save(any(ParkingLog.class));
        verify(vehicleTypeInfoRepository, times(0)).save(any(VehicleTypeInfo.class));
        assertThat(Objects.requireNonNull(responseEntity.getBody()).getResponse()).isEqualTo(String.format(VEHICLE_NOT_PARKED.getBody(),
                VEHICLE_NUMBER, PARKING_LOT_ID));
        assertThat(responseEntity.getStatusCode()).isEqualTo(VEHICLE_NOT_PARKED.getHttpStatus());
    }

    private ParkingRequestParams getTestRequestParams() {
        return new ParkingRequestParams(PARKING_LOT_ID, VEHICLE_TYPE, VEHICLE_NUMBER, VEHICLE_WEIGHT);
    }

    private VehicleInfoPK getVehicleInfoPrimaryKey(ParkingRequestParams requestParam) {
        VehicleInfoPK pk = new VehicleInfoPK(VehicleType.valueOf(requestParam.getVehicleType()));
        pk.setParkingLot(requestParam.getParkingLotId());
        return pk;
    }

    private VehicleTypeInfo getTestVehicleTypeInfoWithVacancy(int vacancy) {
        VehicleTypeInfo vehicleTypeInfo = new VehicleTypeInfo();
        vehicleTypeInfo.setVacancy(vacancy);
        vehicleTypeInfo.setChargePerHour(CHARGE_PER_HOUR);
        vehicleTypeInfo.setCurrency(CURRENCY);
        return vehicleTypeInfo;
    }

    private ParkingLog getTestParkinglog(final ParkingRequestParams parkRequest) {
        return ParkingLog.builder()
                .enterTimeEpoch(TEST_DATE.getTime())
                .exitTimeEpoch(EMPTY_EXIT_TIME)
                .parkingLotId(parkRequest.getParkingLotId())
                .vehicleNumber(parkRequest.getVehicleNumber())
                .vehicleType(VehicleType.valueOf(parkRequest.getVehicleType()))
                .vehicleWeight(parkRequest.getVehicleWeight())
                .build();
    }

}